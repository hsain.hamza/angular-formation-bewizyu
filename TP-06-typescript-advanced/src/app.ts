
// Apply, Call and Bind 
const obj = {
    name : 'Hello',
    displayName(){
        console.log('Object => ', this);
    }
}
class User {
    displayName(){
        console.log('Class => ' , this);
    }
}
function displayName(arg1: string, arg2: string, arg3: string){
    console.log('arg1 => ', arg1);
    console.log('arg2 => ', arg2);
    console.log('arg3 => ', arg3);
    console.log('Function => ', this);
}



//  Type Queries  

interface LoginRequest {
    username: string;
    password: string;
    isAdmin?: boolean;
    request?: LoginRequest;
}
const loginRequest : LoginRequest = { 
    username : 'TXT', password : 'TXT'
};


//  Create custom type   

let userRole: 'ADMIN' | 'USER' | 'MANAGER';




// Keyof usage 



// console.log(getProperty(loginRequest, 'request'))



// Type guards 

class Vehicule {
    public getVitesse(){
        return 190;
    }
}
let monVehicule : any = new Vehicule();


// in operator 

if('username' in loginRequest) {
    console.log('username in login');
}



