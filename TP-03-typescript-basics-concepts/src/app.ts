// TODO 1 : Arrow Functions

// Transformer la fonction suivante en arrow function 

const newArray = [1, 2, 3, 4, 5, 6].map(function (value) {
    return value + 10;
});


const obj = {
    name: 'Jean',
    callback: function () {
        setTimeout(function () {            
            console.log(this);
        }, 1000)
    }
}
const obj2 = {
    name: 'Dupond',
    callback: () => {
        setTimeout(() => {
            console.log(this);
        }, 1000)
    }
}

// Constater la difference 
// obj.callback();
// obj2.callback();

// TODO 2 : Default parameters 

// Mettre 'email@gmail.com' comme valeur par defaut pour l'attribut email
function addFriend(id: string, email: string) {
    console.log('Friend added : ', id, email)
}

// TODO 3 : Object mapping


// TODO 4 : Rest parameters (Indefinit number of parameters)
function myCustomLog(age: number, args: string[]) {
    // afficher l'ensemble des arguments 
    
    args.forEach(elem => {
        console.log('CUSTOM LOG : ' + new Date(), elem);
    })
}

// TODO 5 : Array Spread operator 

const myTable = [1, 2, 3, 4, 5];
const mySecondTab = [7, 8, 9];

// Fusioner les deux tableaux 


// TODO 6 : Object spread operator

const order = {
    orderId: 232,
    orderName: 'Hello'
}

const user = {
    firstName: 'Jean',
    lastName: 'Dupond',
    orderName: 'Hello 2'
}

const tags = ['tag1', 'tag2'];

// Fusionner l'ensemble des elements 

// TODO 7 : Destructing Arrays and Objects 

const userProfil = {
    image: 'URL DE L\'IMAGE',
    full_name: 'Jean Dupond'
}

function getUserProfile() {
    return userProfil;
}


// 


const userTable = [
    {
        image: 'URL DE L\'IMAGE',
        full_name: 'user 1'
    }, {
        image: 'URL DE L\'IMAGE',
        full_name: 'user 2'
    }, {
        image: 'URL DE L\'IMAGE',
        full_name: 'user 3'
    }
]
 


 


